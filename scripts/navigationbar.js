jQuery(document).ready(function(){

	// Identify and select the current website
	jQuery('.navigationitemlink').each( function() {
		var spanText = jQuery(this).text().toLowerCase();
		if ( document.location.href.indexOf(spanText) != -1) {
			jQuery(this).addClass('navigationitemlinkselected')
		}
	});

	// Set the current image
	var sslToggleImgURL = '';
	if (document.location.protocol == 'http:') {
		sslToggleImgURL = '/shared/lock/img/lock-insecure.png';
	} else {
		sslToggleImgURL = '/shared/lock/img/lock-secure.png';
	}
	jQuery('.sslToggle').css('background', 'transparent url(' + sslToggleImgURL + ') no-repeat top left');


	// Identify and register the sslToggle target link
	var newSSLToggleURL = '';
	if (document.location.protocol == 'http:') {
		newSSLToggleURL = document.location.href.replace(/^http/, 'https');
	} else {
		newSSLToggleURL = document.location.href.replace(/^https/, 'http');
	}

	// Check whether the destination website /shared/misc/empty.json can be loaded.
	// if yes (http or https w/ certificate), print the simple toggle
	// if no (https without certificate), print display the warning message

	function sslToggleMessageYes() {
		jQuery('.sslToggle').on('mouseover', function() {
			jQuery('#sslToggleMessage').show();
		});
		jQuery('#sslToggleMessage').on('mouseleave', function() {
			jQuery('#sslToggleMessage').hide();
		});
		jQuery('#sslLink').click(function() {
			jQuery(this).attr('href', newSSLToggleURL);
		});
	}

	function sslToggleMessageNo() {
		jQuery('.sslToggle').click(function() {
			document.location = newSSLToggleURL;
		});
	}

	function emptyOov0jaem() {}

	if (document.location.protocol == 'http:') {
		var a = jQuery('<a>', { href:newSSLToggleURL } )[0];
		var testUrl = a.protocol+"//"+a.hostname+"/shared/misc/empty.json"; 
		var request = jQuery.ajax({
			url: testUrl,
			dataType: "jsonp",
			jsonpCallback: "emptyOov0jaem",
			timeout: 2000,
		});
		request.done(sslToggleMessageNo);
		request.fail(sslToggleMessageYes);
	} else {
		sslToggleMessageNo();
	}

});
